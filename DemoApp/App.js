import React, { Component } from 'react';
import {
  SafeAreaView,
  ScrollView,
  NativeModules,
  NativeEventEmitter,
  Text,
  View,
  Button,
  TouchableOpacity
} from 'react-native';

export default class App extends Component {

  constructor() {
    super();
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      var iosNotifyManagerEmitter = new NativeEventEmitter(NativeModules.ISGNativeModule);
      iosNotifyManagerEmitter.addListener('Pg_Status', (event) => {
        console.log(event);
        console.log("here is Pg_Status");
      });
    } 
   }
 

  render() {

    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
        >
          <View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: "center", marginTop: 45 }}>
              <Button
                title="InitSDK"
                onPress={() => {
                  if (Platform.OS === 'ios') {                
                      NativeModules.ISGNativeModule.initSDK("5A95E0B7301E5D253170A35C6F4A0B1D",
                        "A3C8B88F8D5D936970B334A9AA2539A7",
                        "IOS_TEST-161017180459",
                        "10000",
                        "ETKT4295",
                        "001002",
                        "10010186", "100000000010588", 
                        "5137", 
                        "356",
                         "pay",
                          "6075000000000023",
                           "a@mail.com", 
                           "123456789",
                            "",
                        "4012001037141112", "012020", "123", "", "", "", "", "", "", "", "1", "UAT",
                      );
                  } else if (Platform.OS == 'android') {

                  }
                }}
              />

            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
