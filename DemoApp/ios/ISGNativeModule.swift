//
//  ISGNativeModule.swift
//  DemoApp
//
//  Created by Apple on 16/08/22.
//

import Foundation
import UIKit


@objc(ISGNativeModule)
class ISGNativeModule: RCTEventEmitter,ISGPayControllerDelegate {
  
  
  //@objc(secureSecret:aesKey:)
        //:txnRefNo:amount:passCode:bankId:terminalId:merchantId:mCC:currency:txnType:orderInfo:email:phone:payOpt:cardNumber:expiryDate:cardSecurityCode:bankCode:firstName:lastName:street:city:zIP:state:version:environment:)
  
  @objc func initSDK(_ secureSecret: String,aesKey: String,txnRefNo : String , amount : String,passCode : String ,bankId : String,terminalId : String,merchantId : String,mCC : String,currency : String,txnType : String,orderInfo : String,email : String,phone : String,payOpt : String,cardNumber : String,expiryDate : String,cardSecurityCode : String,bankCode : String ,firstName : String,lastName : String,street : String,city : String,zIP : String,state : String,version : String,environment : String) -> Void {
        
      let reqObj = ISGPayRequest(secureSecret: secureSecret, aesKey: aesKey)!
      reqObj.environment = environment;
      reqObj.txnRefNo = txnRefNo;
      reqObj.amount = amount;
      reqObj.passCode = passCode;
      reqObj.bankId = bankId;
      reqObj.terminalId = terminalId;
      reqObj.merchantId = merchantId;
      reqObj.mcc = mCC;
      reqObj.currency = currency;
      reqObj.txnType = txnType;
      reqObj.orderInfo = orderInfo;
      reqObj.payOpt = payOpt
      reqObj.cardNumber = cardNumber;
      reqObj.expiryDate = expiryDate;
      reqObj.cardSecurityCode = cardSecurityCode;
      reqObj.version = version;
    
    DispatchQueue.main.async {
      let topController = UIApplication.topMostViewController()
      ISGPayController.initiateISGPay(with: reqObj, for:topController, delegate: self).makePayment()
    }
  }
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  override func supportedEvents() -> [String]! {
    return ["Pg_Status"]
  }
  
  func didFinishPayment(withData dictData: [AnyHashable : Any]!) {
    print("done")
    sendEvent(withName: "Pg_Status", body: ["data": dictData!,"status" : "complete","errorDesc" : ""])
    
  }
  
  func didFailWithError(_ error: Error!) {
    print("error")
    sendEvent(withName: "Pg_Status", body: ["data": [:],"status" : "error","errorDesc" :error.debugDescription])
    
  }
}

extension UIApplication {
  class func topMostViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
    if let navigationController = controller as? UINavigationController {
      return topMostViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
      if let selected = tabController.selectedViewController {
        return topMostViewController(controller: selected)
      }
    }
    if let presented = controller?.presentedViewController {
      return topMostViewController(controller: presented)
    }
    return controller
  }
}
