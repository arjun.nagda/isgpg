//
//  Transaction.h
//  ISGPayUI
//
//  Created by isg on 10/14/16.
//  Copyright © 2016 ISG. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface ISGPayRequest : NSObject

@property (nonatomic) NSString* TxnRefNo;  //Alphanumeric
@property (nonatomic) NSString* Amount;  //Numeric
@property (nonatomic) NSString* PassCode;  //Alphanumeric
@property (nonatomic) NSString* BankId;  //Alphanumeric
@property (nonatomic) NSString* TerminalId;  //Alphanumeric
@property (nonatomic) NSString* MerchantId; //Alphanumeric
@property (nonatomic) NSString* MCC; //Numeric
@property (nonatomic) NSString* Currency; //Numeric
@property (nonatomic) NSString* TxnType; //Alphabetic
@property (nonatomic) NSString* OrderInfo; //Numeric
@property (nonatomic) NSString* Email; //Alphanumeric
@property (nonatomic) NSString* Phone; //Numeric
@property (nonatomic) NSString* payOpt; //Alphabetic
@property (nonatomic) NSString* CardNumber; //Numeric
@property (nonatomic) NSString* ExpiryDate; //Numeric
@property (nonatomic) NSString* CardSecurityCode; //Numeric
@property (nonatomic) NSString* BankCode; //Alphanumeric
@property (nonatomic) NSString* FirstName; //Alphabetic
@property (nonatomic) NSString* LastName; //Alphabetic
@property (nonatomic) NSString* Street; //Alphabetic
@property (nonatomic) NSString* City; //Alphabetic
@property (nonatomic) NSString* ZIP; //Numeric
@property (nonatomic) NSString* State; //Alphabetic
@property (nonatomic) NSString* Version;

@property (nonatomic) NSString* Environment;





- (NSString*)SecureSecret;
- (NSString*)AESKey;


- (id)initWithSecureSecret:(NSString*)SecureSecret AESKey:(NSString*)aeskey;

@end



