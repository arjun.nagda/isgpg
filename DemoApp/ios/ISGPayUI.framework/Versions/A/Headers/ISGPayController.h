//
//  ISGPayController.h
//  ISGDevApp
//
//  Created by isg on 10/12/16.
//  Copyright © 2016 ISG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISGPayRequest.h"

@protocol ISGPayControllerDelegate <NSObject>

@required
-(void)didFinishPaymentWithData:(NSDictionary*)dictData;
-(void)didFailWithError:(NSError*)error;


@end


@interface ISGPayController : UIViewController

@property (nonatomic) ISGPayRequest *payReqObj;
@property (nonatomic) id<ISGPayControllerDelegate>delegate;
+(ISGPayController*)initiateISGPayWithRequest:(ISGPayRequest*)payReq forViewController:(UIViewController*)currentViewController delegate:(id)delegate;
-(void)MakePayment;

@end

