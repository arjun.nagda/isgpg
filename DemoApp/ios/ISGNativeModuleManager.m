//
//  DemoHandler.m
//  DemoApp
//
//  Created by Apple on 17/08/22.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(ISGNativeModule, NSObject)
//RCT_EXTERN_METHOD(addEvent:(NSString *)name)
RCT_EXTERN_METHOD(initSDK:(NSString *)secureSecret aesKey:(NSString *)aesKey txnRefNo:(NSString *)txnRefNo amount:(NSString *)amount passCode:(NSString *)passCode bankId:(NSString *)bankId terminalId:(NSString *)terminalId merchantId:(NSString *)merchantId mCC:(NSString *)mCC currency:(NSString *)currency txnType:(NSString *)txnType orderInfo:(NSString *)orderInfo email:(NSString *)email phone:(NSString *)phone payOpt:(NSString *)payOpt cardNumber:(NSString *)cardNumber expiryDate:(NSString *)expiryDate cardSecurityCode:(NSString *)cardSecurityCode bankCode:(NSString *)bankCode firstName:(NSString *)firstName lastName:(NSString *)lastName street:(NSString *)street city:(NSString *)city zIP:(NSString *)zIP state:(NSString *)state version:(NSString *)version environment:(NSString *)environment)


@end

