#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>
#import <ISGPayUI/ISGPayUI.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate>
@property (nonatomic, strong) UIWindow *window;

@end
